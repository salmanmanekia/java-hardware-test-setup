package test;

public class ShoppingItem {
 private int sWarehouseId;
 private String sManufacturer;
 private String sModel;
 private int lWarehouseId;
 private String lManufacturer;
 private String lModel;
 private int ldWarehouseId;
 private String ldManufacturer;
 private String ldModel;
 private int sdWarehouseId;
 private String sdManufacturer;
 private String sdModel;
 private int sCWarehouseId;
 private String sCManufacturer;
 private String sCModel;

 public ShoppingItem() {
  this.sWarehouseId = 0;
  this.sManufacturer = null;
  this.sModel = null;
  this.lWarehouseId = 0;
  this.lManufacturer = null;
  this.lModel = null;
  this.ldWarehouseId = 0;
  this.ldManufacturer = null;
  this.ldModel = null;
  this.sdWarehouseId = 0;
  this.sdManufacturer = null;
  this.sdModel = null;
  this.sCWarehouseId = 0;
  this.sCManufacturer = null;
  this.sCModel = null;
 }

 public int getsWarehouseId() {
  return sWarehouseId;
 }
 public void setsWarehouseId(int sWarehouseId) {
  this.sWarehouseId = sWarehouseId;
 }
 public String getsManufacturer() {
  return sManufacturer;
 }
 public void setsManufacturer(String sManufacturer) {
  this.sManufacturer = sManufacturer;
 }
 public String getsModel() {
  return sModel;
 }
 public void setsModel(String sModel) {
  this.sModel = sModel;
 }
 public int getlWarehouseId() {
  return lWarehouseId;
 }
 public void setlWarehouseId(int lWarehouseId) {
  this.lWarehouseId = lWarehouseId;
 }
 public String getlManufacturer() {
  return lManufacturer;
 }
 public void setlManufacturer(String lManufacturer) {
  this.lManufacturer = lManufacturer;
 }
 public String getlModel() {
  return lModel;
 }
 public void setlModel(String lModel) {
  this.lModel = lModel;
 }
 public int getsdWarehouseId() {
  return sdWarehouseId;
 }
 public void setsdWarehouseId(int sdWarehouseId) {
  this.sdWarehouseId = sdWarehouseId;
 }
 public String getsdManufacturer() {
  return sdManufacturer;
 }
 public void setsdManufacturer(String sdManufacturer) {
  this.sdManufacturer = sdManufacturer;
 }
 public String getsdModel() {
  return sdModel;
 }
 public void setsdModel(String sdModel) {
  this.sdModel = sdModel;
 }

 public int getldWarehouseId() {
  return ldWarehouseId;
 }
 public void setldWarehouseId(int ldWarehouseId) {
  this.ldWarehouseId = ldWarehouseId;
 }
 public String getldManufacturer() {
  return ldManufacturer;
 }
 public void setldManufacturer(String ldManufacturer) {
  this.ldManufacturer = ldManufacturer;
 }
 public String getldModel() {
  return ldModel;
 }
 public void setldModel(String ldModel) {
  this.ldModel = ldModel;
 }
 public int getsCWarehouseId() {
  return sCWarehouseId;
 }
 public void setsCWarehouseId(int sCWarehouseId) {
  this.sCWarehouseId = sCWarehouseId;
 }
 public String getsCManufacturer() {
  return sCManufacturer;
 }
 public void setsCManufacturer(String sCManufacturer) {
  this.sCManufacturer = sCManufacturer;
 }
 public String getsCModel() {
  return sCModel;
 }
 public void setsCModel(String sCModel) {
  this.sCModel = sCModel;
 }
}
