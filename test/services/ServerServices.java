package test.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import test.model.ServerInfo;
import test.model.StorageControllerInfo;
import test.util.ConfigProperty;

public class ServerServices {
 private ArrayList<ServerInfo> sInfoList = new ArrayList<ServerInfo>(); 
 int busPCI = 0;

 public ServerServices () throws IOException {
  this.loadServerInfos();
 }

 private void loadServerInfos() throws IOException {
  ConfigProperty.getInstance();
  String path = ConfigProperty.getProp().getProperty("serverfile");
  BufferedReader serverCsv = new BufferedReader (
    new InputStreamReader(
      ServerServices.class.getClassLoader().getResourceAsStream(path)));
  int count = 0;
  serverCsv.readLine();
  String dataRow = serverCsv.readLine();
  ServerInfo sInfo;
  while (dataRow != null){
   sInfo= new ServerInfo();
   String[] dataArray = dataRow.split(",");
   for (String item:dataArray) {
    if (count == 0) sInfo.setWareHouseID(Integer.parseInt(item));
    if (count == 1) sInfo.setManufacturer(item);
    if (count == 2) sInfo.setModel(item);
    if (count == 3) sInfo.setProcessor(item);
    if (count == 4) sInfo.setMemory(item);
    if (count == 5) sInfo.setPciBuses(Integer.parseInt(item));
    if (count == 6) sInfo.setPciXBus(Integer.parseInt(item));
    if (count == 7) sInfo.setPciExBus(Integer.parseInt(item));
    ++count;
   }
   count = 0;
   sInfoList.add(sInfo);
   dataRow = serverCsv.readLine(); // Read next line of data.
  }
  serverCsv.close();
 }

 public ArrayList<ServerInfo> getServerInfos() throws IOException {
  if (sInfoList == null) {
   loadServerInfos();
  }
  return sInfoList;
 }

 public boolean validate(ServerInfo si, List<StorageControllerInfo> sCIList) {

  int pciBus = si.getPciBuses();

  for (StorageControllerInfo scii:sCIList) {
   String scBusPci = scii.getBus();
   if (scBusPci.equals("PCI")) {
    this.busPCI++;
   }
   if (pciBus < this.busPCI){ 
    this.busPCI--;
    return false;
   }
  }
  return true;
 }
}
