package test.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import test.model.DiskInfo;
import test.util.ConfigProperty;

public class DiskServices {

 private ArrayList <DiskInfo> dInfoList = new ArrayList <DiskInfo> ();

 public DiskServices() throws IOException {
  this.loadDiskInfos();
 }

 private void loadDiskInfos() throws IOException {
  ConfigProperty.getInstance();
  String path = ConfigProperty.getProp().getProperty("diskfile");
  BufferedReader diskCsv = new BufferedReader (
    new InputStreamReader(
      ServerServices.class.getClassLoader().getResourceAsStream(path)));
  int count = 0;
  diskCsv.readLine();
  String dataRow = diskCsv.readLine();
  DiskInfo dInfo;
  while (dataRow != null){
   dInfo = new DiskInfo();
   String[] dataArray = dataRow.split(",");
   for (String item:dataArray) {
    if (count == 0) dInfo.setWareHouseID(Integer.parseInt(item));
    if (count == 1) dInfo.setManufacturer(item);
    if (count == 2) dInfo.setModel(item);
    if (count == 3) dInfo.setSize(item);
    if (count == 4) dInfo.setPort(item);
    ++count;
   }
   count = 0;
   dInfoList.add(dInfo);
   dataRow = diskCsv.readLine(); // Read next line of data.
  }
  diskCsv.close();
 }

 public ArrayList<DiskInfo> getDiskInfos() throws IOException {
  if (dInfoList == null) {
   loadDiskInfos();
  }
  return dInfoList;
 }
}
