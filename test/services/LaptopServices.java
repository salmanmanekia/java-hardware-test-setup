package test.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import test.model.DiskInfo;
import test.model.LaptopInfo;
import test.util.ConfigProperty;

public class LaptopServices {
 private ArrayList<LaptopInfo> lInfoList = new ArrayList<LaptopInfo>(); 

 public LaptopServices() throws IOException {
  this.loadLaptopInfos(); 
 }

 private void loadLaptopInfos() throws IOException {
  ConfigProperty.getInstance();
  String path = ConfigProperty.getProp().getProperty("laptopfile");
  BufferedReader laptopCsv =  new BufferedReader (
    new InputStreamReader(
      ServerServices.class.getClassLoader().getResourceAsStream(path)));
  int count = 0;
  laptopCsv.readLine();
  String dataRow = laptopCsv.readLine();
  LaptopInfo lInfo;
  while (dataRow != null){
   lInfo= new LaptopInfo();
   String[] dataArray = dataRow.split(",");
   for (String item:dataArray) {
    if (count == 0) lInfo.setWareHouseID(Integer.parseInt(item));
    if (count == 1) lInfo.setManufacturer(item);
    if (count == 2) lInfo.setModel(item);
    if (count == 3) lInfo.setProcessor(item);
    if (count == 4) lInfo.setMemory(item);
    if (count == 5) lInfo.setDiskPorts(Integer.parseInt(item));

    ++count;
   }
   count = 0;
   lInfoList.add(lInfo);
   dataRow = laptopCsv.readLine(); // Read next line of data.
  }
  laptopCsv.close();
 }

 public ArrayList<LaptopInfo> getLaptopInfos() throws IOException {
  if ( lInfoList== null) {
   loadLaptopInfos();
  }
  return lInfoList;
 }

 public boolean validate (DiskInfo di) {
  if (di.getPort().equals("SATA")) return true;
  return false;
 }
}


