package test.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import test.model.DiskInfo;
import test.model.StorageControllerInfo;
import test.util.ConfigProperty;

public class StorageServices {

 private ArrayList<StorageControllerInfo> sCInfoList = new ArrayList<StorageControllerInfo>(); 

 public StorageServices() throws IOException {
  this.loadStorageInfos();
 }

 public void loadStorageInfos() throws IOException {
  ConfigProperty.getInstance();
  String path = ConfigProperty.getProp().getProperty("storagefile");
  BufferedReader storageCsv =  new BufferedReader (
    new InputStreamReader(
      ServerServices.class.getClassLoader().getResourceAsStream(path)));
  int count = 0;
  storageCsv.readLine();
  String dataRow = storageCsv.readLine();
  StorageControllerInfo sCInfo;
  while (dataRow != null){
   sCInfo= new StorageControllerInfo();
   String[] dataArray = dataRow.split(",");
   for (String item:dataArray) {
    if (count == 0) sCInfo.setWareHouseID(Integer.parseInt(item));
    if (count == 1) sCInfo.setManufacturer(item);
    if (count == 2) sCInfo.setModel(item);
    if (count == 3) sCInfo.setBus(item);
    if (count == 4) sCInfo.setDiskPortType(item);
    if (count == 5) sCInfo.setDiskPorts(Integer.parseInt(item));
    ++count;
   }
   count = 0;
   sCInfoList.add(sCInfo);
   dataRow = storageCsv.readLine(); // Read next line of data.
  }
  storageCsv.close();

 }

 public ArrayList<StorageControllerInfo> getStorageControllerInfos() throws IOException {
  if (sCInfoList == null) {
   loadStorageInfos();
  }
  return sCInfoList;
 }

 public boolean validate(StorageControllerInfo sci, DiskInfo di) {

  String controllerDiskPort = sci.getDiskPortType();
  String diskPortType = di.getPort();
  if (controllerDiskPort.equals("SCSI")) {
   if (diskPortType.equals("SCSI")) return true;
   return false;
  }
  if (controllerDiskPort.equals("SAS")) {
   if(diskPortType.equals("SAS") || diskPortType.equals("SATA")) return true;
   return false;
  }
  if (controllerDiskPort.equals("SATA")) {
   if(diskPortType.equals("SATA")) return true;
   return false;
  }

  return true;
 }
}
