package test.model;

public class DiskInfo {
 private int wareHouseID;
 private String manufacturer;
 private String model;
 private String size;
 private String port;

 public int getWareHouseID() {
  return wareHouseID;
 }
 public void setWareHouseID(int wareHouseID) {
  this.wareHouseID = wareHouseID;
 }
 public String getManufacturer() {
  return manufacturer;
 }
 public void setManufacturer(String manufacturer) {
  this.manufacturer = manufacturer;
 }
 public String getModel() {
  return model;
 }
 public void setModel(String model) {
  this.model = model;
 }
 public String getSize() {
  return size;
 }
 public void setSize(String size) {
  this.size = size;
 }
 public String getPort() {
  return port;
 }
 public void setPort(String port) {
  this.port = port;
 } 
}






