package test.model;

public class ServerInfo {
 private int wareHouseID;
 private String manufacturer;
 private String model;
 private String processor;
 private String memory;
 private int pciBuses;
 private int pciXBus;
 private int pciExBus;

 public int getWareHouseID() {
  return wareHouseID;
 }
 public void setWareHouseID(int wareHouseID) {
  this.wareHouseID = wareHouseID;
 }
 public String getManufacturer() {
  return manufacturer;
 }
 public void setManufacturer(String manufacturer) {
  this.manufacturer = manufacturer;
 }
 public String getModel() {
  return model;
 }
 public void setModel(String model) {
  this.model = model;
 }
 public String getProcessor() {
  return processor;
 }
 public void setProcessor(String processor) {
  this.processor = processor;
 }
 public String getMemory() {
  return memory;
 }
 public void setMemory(String memory) {
  this.memory = memory;
 }
 public int getPciBuses() {
  return pciBuses;
 }
 public void setPciBuses(int pciBuses) {
  this.pciBuses = pciBuses;
 }
 public int getPciXBus() {
  return pciXBus;
 }
 public void setPciXBus(int pciXBus) {
  this.pciXBus = pciXBus;
 }
 public int getPciExBus() {
  return pciExBus;
 }
 public void setPciExBus(int pciExBus) {
  this.pciExBus = pciExBus;
 }
}
