package test.model;

public class StorageControllerInfo {

 private int wareHouseID;
 private String manufacturer;
 private String model;
 private String bus;
 private String diskPortType;
 private int diskPorts;

 public int getWareHouseID() {
  return wareHouseID;
 }
 public void setWareHouseID(int wareHouseID) {
  this.wareHouseID = wareHouseID;
 }
 public String getManufacturer() {
  return manufacturer;
 }
 public void setManufacturer(String manufacturer) {
  this.manufacturer = manufacturer;
 }
 public String getModel() {
  return model;
 }
 public void setModel(String model) {
  this.model = model;
 }
 public String getBus() {
  return bus;
 }
 public void setBus(String bus) {
  this.bus = bus;
 }
 public String getDiskPortType() {
  return diskPortType;
 }
 public void setDiskPortType(String diskPortType) {
  this.diskPortType = diskPortType;
 }
 public int getDiskPorts() {
  return diskPorts;
 }
 public void setDiskPorts(int diskPorts) {
  this.diskPorts = diskPorts;
 }
}
