package test.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperty {

 private static ConfigProperty configProperty = new ConfigProperty();
 private static Properties prop;

 public static Properties getProp() {
  return prop;
 }

 public static ConfigProperty getInstance() throws IOException {
  return configProperty;
 }

 private ConfigProperty() {
  prop = new Properties();
  InputStream in = getClass().getResourceAsStream(
    "/test/util/CSV.properties");
  try {
   prop.load(in);
   in.close();
  } 
  catch (IOException e) {
   e.printStackTrace();
  }
 }
}
