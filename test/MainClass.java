package test;

import java.io.File;
import java.io.IOException;

import test.services.DiskServices;
import test.services.LaptopServices;
import test.services.ServerServices;
import test.services.StorageServices;
import test.ui.UserInterface;
import test.util.ConfigProperty;

public class MainClass {

 public static void main(String[] args) throws IOException {
  System.out.println(new File(".").getCanonicalPath());
  ConfigProperty.getInstance();
  new ServerServices();
  new DiskServices();
  new StorageServices();
  new LaptopServices();
  new UserInterface();
 }

}
