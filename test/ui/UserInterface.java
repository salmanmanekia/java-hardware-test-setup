package test.ui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import test.ShoppingItem;
import test.model.DiskInfo;
import test.model.LaptopInfo;
import test.model.ServerInfo;
import test.model.StorageControllerInfo;
import test.services.DiskServices;
import test.services.LaptopServices;
import test.services.ServerServices;
import test.services.StorageServices;

public class UserInterface implements ActionListener{

 private JPanel baseUnitPanel, storageControllerPanel, diskPanel, buttonPanel;
 private JLabel baseUnitLabel, storageControllerLabel, diskLabel, descriptionLabel;
 private JRadioButton serverButton, laptopButton;
 private JComboBox serverBox, laptopBox, storageControllerBox, diskBox;
 private JTextField descriptionField;
 private JButton addSc, addDisk, checkOutButton;
 private ButtonGroup buttonGroup;
 private int serverChanged, laptopChanged;
 private String serverSelected, laptopSelected, diskSelected, sCSelected;
 private List<ShoppingItem> shoppingItem;
 private ShoppingItem item;

 private ServerServices ss;
 private LaptopServices ls;
 private StorageServices scs;
 private DiskServices ds;
 private List<ServerInfo> sIList;
 private List<LaptopInfo> lIList;
 private List<StorageControllerInfo> sCIList;
 private List<DiskInfo> dServerList;
 private List<DiskInfo> dLaptopList;

 private ServerInfo si;
 private StorageControllerInfo sci;

 JFrame frame;

 public UserInterface () throws IOException {

  serverChanged = 0;
  laptopChanged = 0;

  ss = new ServerServices();
  ls = new LaptopServices();
  scs = new StorageServices();
  ds = new DiskServices();

  si = new ServerInfo();
  sci = new StorageControllerInfo();

  sIList = new ArrayList<ServerInfo>();
  lIList = new ArrayList<LaptopInfo>();
  sCIList = new ArrayList<StorageControllerInfo>();
  dServerList = new ArrayList<DiskInfo>();
  dLaptopList =new ArrayList<DiskInfo>();

  shoppingItem = new ArrayList<ShoppingItem>();
  baseUnitPanel = new JPanel();
  storageControllerPanel = new JPanel();
  diskPanel = new JPanel();
  buttonPanel = new JPanel();

  shoppingItem = new ArrayList <ShoppingItem>();
  item = new ShoppingItem();

  baseUnitLabel = new JLabel("Base Unit ");
  storageControllerLabel = new JLabel("Storage Controller");
  diskLabel = new JLabel("Disk");
  descriptionLabel = new JLabel("Description");

  serverBox = new JComboBox();
  serverBox.setEnabled(false);
  laptopBox = new JComboBox ();
  laptopBox.setEnabled(false);
  storageControllerBox = new JComboBox();
  storageControllerBox.setEnabled(false);
  diskBox = new JComboBox();
  diskBox.setEnabled(false);

  serverButton = new JRadioButton("Server ");
  serverButton.setSelected(false);
  laptopButton = new JRadioButton("Laptop ");
  buttonGroup = new ButtonGroup();

  descriptionField = new JTextField();

  addSc = new JButton("Add Storage");
  addSc.setEnabled(false);
  addDisk = new JButton("Add Disk");
  addDisk.setEnabled(false);
  checkOutButton = new JButton("Check Out & View the Shopping list");
  checkOutButton.setEnabled(false);

  SwingUtilities.invokeLater(new Runnable() {
   public void run() {
    createAndShowGUI();
   }
  });

  serverButton.addActionListener (new ActionListener() {
   public void actionPerformed(ActionEvent ae){
    ++serverChanged;

    serverBox.setEnabled(true);
    laptopBox.setEnabled(false);
    storageControllerBox.setEnabled(true);
    addSc.setEnabled(true);
    diskBox.setEnabled(true);
    addDisk.setEnabled(true);
   }

  });

  laptopButton.addActionListener (new ActionListener() {
   public void actionPerformed(ActionEvent ae){
    ++laptopChanged;
    laptopBox.setEnabled(true);
    serverBox.setEnabled(false);
    storageControllerBox.setEnabled(false);
    addSc.setEnabled(false);
    diskBox.setEnabled(true);
    addDisk.setEnabled(true);
   }
  });

  for (ServerInfo sii: ss.getServerInfos()) {
   serverBox.addItem(sii.getModel());
  }

  for (LaptopInfo lii: ls.getLaptopInfos()) {
   laptopBox.addItem(lii.getModel());
  }

  for (StorageControllerInfo scii: scs.getStorageControllerInfos()) {
   storageControllerBox.addItem(scii.getModel());
  }

  for (DiskInfo dii: ds.getDiskInfos()) {
   diskBox.addItem(dii.getModel());
  }

  serverBox.addItemListener(new ItemListener(){
   public void itemStateChanged(ItemEvent ie){

    ++serverChanged;
    storageControllerBox.setEnabled(true);
    addSc.setEnabled(true);
    diskBox.setEnabled(true);
    addDisk.setEnabled(true);

    descriptionField.setText(null);
    String model = (String)serverBox.getSelectedItem();
    int warehouseId = 0;
    String manufacturer = null;
    try {
     for (ServerInfo si: ss.getServerInfos()) {
      if (model.equals(si.getModel()) ) {
       warehouseId = si.getWareHouseID();
       manufacturer = si.getManufacturer();
       continue;
      }
     }
    } catch (IOException e) {
     e.printStackTrace();
    }
    descriptionField.setText("Warehouse ID: " + warehouseId +  " Manufacturer : " + manufacturer + " Model : " + model );
   }
  });

  laptopBox.addItemListener(new ItemListener(){
   public void itemStateChanged(ItemEvent ie){

    ++laptopChanged;
    diskBox.setEnabled(true);
    addDisk.setEnabled(true);
    descriptionField.setText(null);
    String model = (String)laptopBox.getSelectedItem();
    int warehouseId = 0;
    String manufacturer = null;

    try {
     for (LaptopInfo li: ls.getLaptopInfos()) {
      if (model.equals(li.getModel()) ) {
       warehouseId = li.getWareHouseID();
       manufacturer = li.getManufacturer();
       continue;
      }
     }
    } catch (IOException e) {
     e.printStackTrace();
    }
    descriptionField.setText("Warehouse ID: " + warehouseId +  " Manufacturer : " + manufacturer + " Model : " + model );
   }
  });

  storageControllerBox.addItemListener(new ItemListener(){
   public void itemStateChanged(ItemEvent ie){
    descriptionField.setText(null);
    String model = (String)storageControllerBox.getSelectedItem();
    int warehouseId = 0;
    String manufacturer = null;

    try {
     for (StorageControllerInfo sci: scs.getStorageControllerInfos()) {
      if (model.equals(sci.getModel()) ) {
       warehouseId = sci.getWareHouseID();
       manufacturer = sci.getManufacturer();
       continue;
      }
     }
    } catch (IOException e) {
     e.printStackTrace();
    }
    descriptionField.setText("Warehouse ID: " + warehouseId +  " Manufacturer : " + manufacturer + " Model : " + model );
   }
  });


  diskBox.addItemListener(new ItemListener(){
   public void itemStateChanged(ItemEvent ie){
    descriptionField.setText(null);
    String model = (String)diskBox.getSelectedItem();
    int warehouseId = 0;
    String manufacturer = null;
    try {
     for (DiskInfo di: ds.getDiskInfos()) {
      if (model.equals(di.getModel()) ) {
       warehouseId = di.getWareHouseID();
       manufacturer = di.getManufacturer();
       continue;
      }
     }
    } catch (IOException e) {
     e.printStackTrace();
    }
    descriptionField.setText("Warehouse ID: " + warehouseId +  " Manufacturer : " + manufacturer + " Model : " + model );
   }
  });

  addSc.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent evt) {
    serverSelected = (String)serverBox.getSelectedItem();
    sCSelected = (String)storageControllerBox.getSelectedItem();
    boolean valid =true;
    item = new ShoppingItem();
    checkOutButton.setEnabled(true);

    try {
     for (ServerInfo sii: ss.getServerInfos()) {
      if (serverSelected.equals(sii.getModel())  && (serverChanged > 0)) {
       si = sii;
       sIList.add(sii);
       item.setsManufacturer(sii.getManufacturer());
       item.setsModel(sii.getModel());
       item.setsWarehouseId(sii.getWareHouseID());
       shoppingItem.add(item);
       serverChanged = 0;
      }
     }
     item = new ShoppingItem();
     for (StorageControllerInfo scii: scs.getStorageControllerInfos()) {
      if (sCSelected.equals(scii.getModel() ) ) {
       sCIList.add(scii);
       valid = ss.validate(si, sCIList);
       if (valid == false) {
        JOptionPane.showMessageDialog(null,"Storage control with BUS " +
          "PCI more than what is allowed with this type of Server");
        sCIList.remove(scii);
       }
       else {
        item.setsCManufacturer(scii.getManufacturer());
        item.setsCModel(scii.getModel());
        item.setsCWarehouseId(scii.getWareHouseID());
        shoppingItem.add(item);
       }
      }       
     }

    } catch (IOException e) {
     e.printStackTrace();
    }
   }
  });

  addDisk.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent evt) {
    checkOutButton.setEnabled(true);

    if (serverButton.isSelected()) {
     sCSelected = (String)storageControllerBox.getSelectedItem();
     diskSelected = (String)diskBox.getSelectedItem();
     item = new ShoppingItem();
     boolean valid = true;
     try {
      for (StorageControllerInfo scii: scs.getStorageControllerInfos()) {
       if (sCSelected.equals(scii.getModel())) {
        sci = scii;
       }
      }
      for (DiskInfo dii: ds.getDiskInfos()) {
       if (diskSelected.equals(dii.getModel())) {
        valid = scs.validate(sci, dii);
        if (valid) {
         dServerList.add(dii);
         item.setsdManufacturer(dii.getManufacturer());
         item.setsdModel(dii.getModel());
         item.setsdWarehouseId(dii.getWareHouseID());
         shoppingItem.add(item);

        }
        else {
         JOptionPane.showMessageDialog(null,"Disk port and Disk is not compatible (Compatible: " +
           "SCSI -> SCSI, SAS -> (SAS & SATA), SATA -> SATA )");
         break;
        }
       }
      }
     }
     catch (IOException e) {
      e.printStackTrace();
     }
    }
    if (laptopButton.isSelected()) {
     laptopSelected = (String)laptopBox.getSelectedItem();
     diskSelected = (String)diskBox.getSelectedItem();
     item = new ShoppingItem();
     boolean valid = true;
     try {
      for (LaptopInfo lii: ls.getLaptopInfos()) {
       if (laptopSelected.equals(lii.getModel()) && (laptopChanged > 0)) {
        lIList.add(lii);
        item.setlManufacturer(lii.getManufacturer());
        item.setlModel(lii.getModel());
        item.setlWarehouseId(lii.getWareHouseID());
        shoppingItem.add(item);
        laptopChanged = 0;
       }
      }
      item = new ShoppingItem();
      for (DiskInfo dii: ds.getDiskInfos()) {
       if (diskSelected.equals(dii.getModel())) {
        valid = ls.validate(dii);
        if (valid) {
         dLaptopList.add(dii);
         item.setldManufacturer(dii.getManufacturer());
         item.setldModel(dii.getModel());
         item.setldWarehouseId(dii.getWareHouseID());
         shoppingItem.add(item);
        }
        else {
         JOptionPane.showMessageDialog(null,"Choose the disk with correct port. Laptop only accepts SATA disks");
         break;
        }

       }
      }

     }
     catch (IOException e) {
      e.printStackTrace();
     }
    } 
   }
  }
    );


  checkOutButton.addActionListener(new ActionListener() {
   public void actionPerformed(ActionEvent evt) {
    System.out.println("---------------------------------------------------------");
    System.out.println("---------------------*SHOPPING LIST*---------------------");
    System.out.println("---------------------------------------------------------");
    System.out.println();

    System.out.println("Server Details");
    System.out.println("==============");
    printServerDetail (shoppingItem);
    System.out.println();

    System.out.println("Storage Controller Details");
    System.out.println("==========================");
    printStorageControllerDetail(shoppingItem);
    System.out.println();

    System.out.println("Server Disk Details");
    System.out.println("===================");
    printServerDiskDetail(shoppingItem);
    System.out.println();

    System.out.println("Laptop Details");
    System.out.println("==============");
    printLaptopDetail(shoppingItem);
    System.out.println();

    System.out.println("Laptop Disk Details");
    System.out.println("===================");
    printLaptopDiskDetail(shoppingItem);
    System.out.println();

    frame.dispose();
   }
  });

 } 

 protected void printServerDetail(List<ShoppingItem> shoppingItem) {
  for (ShoppingItem shopitem:shoppingItem) {
   if (shopitem.getsWarehouseId() != 0)
    System.out.print ("Warehouse ID : " + shopitem.getsWarehouseId());
   if (shopitem.getsManufacturer() != null)
    System.out.print (", Manufacturer : " + shopitem.getsManufacturer());
   if (shopitem.getsModel() != null)
    System.out.println (", Model : " + shopitem.getsModel());
  } 
 }
 protected void printStorageControllerDetail(List<ShoppingItem> shoppingItem) {
  for (ShoppingItem shopitem:shoppingItem) {
   if (shopitem.getsCWarehouseId() != 0)
    System.out.print("Warehouse ID : " + shopitem.getsCWarehouseId());
   if (shopitem.getsCManufacturer() != null)
    System.out.print (", Manufacturer : " + shopitem.getsCManufacturer());
   if (shopitem.getsCModel() != null)
    System.out.println (", Model : " + shopitem.getsCModel());
  }
 }

 protected void printServerDiskDetail(List<ShoppingItem> shoppingItem) {
  for (ShoppingItem shopitem:shoppingItem) {
   if (shopitem.getsdWarehouseId() != 0)
    System.out.print ("Warehouse ID : " + shopitem.getsdWarehouseId());
   if (shopitem.getsdManufacturer() != null)
    System.out.print (", Manufacturer : " +shopitem.getsdManufacturer());
   if (shopitem.getsdModel() != null)
    System.out.println (", Model : " +shopitem.getsdModel());   
  }

 }

 protected void printLaptopDetail(List<ShoppingItem> shoppingItem) {
  for (ShoppingItem shopitem:shoppingItem) {
   if (shopitem.getlWarehouseId() != 0)
    System.out.print ("Warehouse ID : " + shopitem.getlWarehouseId());
   if (shopitem.getlManufacturer() != null)
    System.out.print (", Manufacturer : "  +shopitem.getlManufacturer());
   if (shopitem.getlModel() != null)
    System.out.println (", Model : " + shopitem.getlModel());
  }
 }

 protected void printLaptopDiskDetail(List<ShoppingItem> shoppingItem) {
  for (ShoppingItem si:shoppingItem) {
   if (si.getldWarehouseId() != 0)
    System.out.print ("Warehouse ID : " + si.getldWarehouseId());
   if (si.getldManufacturer() != null)
    System.out.print (", Manufacturer : "  + si.getldManufacturer());
   if (si.getldModel() != null)
    System.out.println (", Model : " + si.getldModel());
  }
 }  


 public JPanel createContentPane () {

  JPanel totalGUI = new JPanel();
  totalGUI.setLayout(null);

  baseUnitPanel.setLayout(null);
  baseUnitPanel.setLocation(10, 10);
  baseUnitPanel.setSize(565, 30);
  totalGUI.add(baseUnitPanel);

  baseUnitLabel.setLocation(0, 0);
  baseUnitLabel.setSize(70, 30);
  baseUnitPanel.add(baseUnitLabel);

  serverBox.setLocation (80, 0);
  serverBox.setSize(140,30);
  baseUnitPanel.add(serverBox);

  serverButton.setLocation(230, 0);
  serverButton.setSize(70, 30);
  baseUnitPanel.add(serverButton);


  laptopBox.setLocation (310, 0);
  laptopBox.setSize(140,30);
  baseUnitPanel.add(laptopBox);

  laptopButton.setLocation(460, 0);
  laptopButton.setSize(80, 30);
  baseUnitPanel.add(laptopButton);

  buttonGroup.add(serverButton);
  buttonGroup.add(laptopButton);


  storageControllerPanel.setLayout(null);
  storageControllerPanel.setLocation(10, 60);
  storageControllerPanel.setSize(565, 30);
  totalGUI.add(storageControllerPanel);

  storageControllerLabel.setLocation(0,0);
  storageControllerLabel.setSize(110,30);
  storageControllerPanel.add(storageControllerLabel);

  storageControllerBox.setLocation(120, 0);
  storageControllerBox.setSize(150,30);
  storageControllerPanel.add(storageControllerBox);


  addSc.setLocation(280,0);
  addSc.setSize(120,30);
  storageControllerPanel.add(addSc);

  diskPanel.setLayout(null);
  diskPanel.setLocation(10, 110);
  diskPanel.setSize(565, 30);
  totalGUI.add(diskPanel);

  diskLabel.setLocation(0,0);
  diskLabel.setSize(110,30);
  diskPanel.add(diskLabel);

  diskBox.setLocation(120, 0);
  diskBox.setSize(150,30);
  diskPanel.add(diskBox);

  addDisk.setLocation(280,0);
  addDisk.setSize(120,30);
  diskPanel.add(addDisk);

  buttonPanel.setLayout(null);
  buttonPanel.setLocation(10,150);
  buttonPanel.setSize(565, 70);
  totalGUI.add(buttonPanel);

  descriptionLabel.setLocation(0, 0);
  descriptionLabel.setSize(80, 30);
  buttonPanel.add(descriptionLabel);

  descriptionField.setLocation(90, 0);
  descriptionField.setSize(460, 30);
  descriptionField.setEditable(false);
  buttonPanel.add(descriptionField);

  checkOutButton.setLocation(40, 40);
  checkOutButton.setSize(450,30);
  buttonPanel.add(checkOutButton);

  totalGUI.setOpaque(true);
  return totalGUI;
 }


 private void createAndShowGUI() {

  JFrame.setDefaultLookAndFeelDecorated(true);
  frame = new JFrame("[=] HARDWARE TEST SETUP [=]");

  frame.setContentPane(this.createContentPane());

  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  frame.setSize(575, 260);
  frame.setResizable(false);
  frame.setVisible(true);
 }

 @Override
 public void actionPerformed(ActionEvent e) {}

}
