HARDWARE TEST SETUP DESIGNER

1: What it does ??
This application allows the user to create a test setup (shopping list of hardware items) and prints info to the command line

2: List of Hardware item
All the hardware items are listed in CSV files.

3: What the software lets user do ??
Select a base unit (Server/Laptop) 
a.  If Server, then user can add Storage controller(s) 
i.  After adding a Storage controller, user should be allowed to add disk(s) to 
storage controller  
b.  If Laptop, user should be allowed to add disk(s) (Laptop only accepts SATA disks) 

4: Some of the conditions enforced are (valid combinations of hardware)
Server that has 2 PCI buses can have max 2 Storage Controllers with bus PCI 
Storage controller that has Disk port type SCSI can only have SCSI disk connected to it. 
Storage controller or Disk can be added multiple times, since warehouse has a bunch of 
each. 
NOTE! Storage controller with Disk port type SAS can accept both SAS and SATA disks. 

5: Execution !!
MainClass.java has the main function and thats were the program starts.